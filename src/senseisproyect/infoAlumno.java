/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package senseisproyect;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Pablo
 */
public class infoAlumno extends javax.swing.JFrame {
private int id,grupo;
bd cnx=new bd();
    /**
     * Creates new form infoAlumno
     */
    public infoAlumno() {
        initComponents();
       
         //bd cnx=new bd();
      try {
               
               Statement qry=cnx.conectar().createStatement();
               ResultSet rs,rs2; 
               DefaultTableModel dtm;
               ResultSetMetaData ram;
               dtm=(DefaultTableModel)this.jTable1.getModel();
               int filas=jTable1.getRowCount();
            for (int i = 0;filas>i; i++) {
               dtm.removeRow(0); 
            }
            
               rs=qry.executeQuery("select a.nombre,c.nombreclub from alumnos as a join club_alumno using(idalumno) join clubes as c using(idclub) where idalumno="+id);
               ram=rs.getMetaData();
               
               int numeroColumnas = ram.getColumnCount();
               Object[] etiquetas = new Object[numeroColumnas];

                // Se obtiene cada una de las etiquetas para cada columna
                for (int i = 0; i < numeroColumnas; i++)
                {
                  // Nuevamente, para ResultSetMetaData la primera columna es la 1. 
                  etiquetas[i] = ram.getColumnLabel(i + 1); 
                }

                dtm.setColumnIdentifiers(etiquetas);
                
               
               ArrayList<Object[]> data=new ArrayList<>();
               while(rs.next()){
               Object[] rows=new Object[ram.getColumnCount()];
               
               for(int i=0;i<rows.length;i++){
               rows[i]=rs.getObject(i+1);
               
               }
               data.add(rows);
               
               }
               
               for (int i = 0; i < data.size(); i++) {
                   dtm.addRow(data.get(i));
                   
               }
           } catch (SQLException ex) {
               //JOptionPane.showMessageDialog(null, "error de sql");
           }
    }
    public infoAlumno(int id, int grupo){
        
    this.id=id;
    this.grupo=grupo;
    initComponents();
    URL url=getClass().getResource("/imagenes/cuyiconopro.ico");
    ImageIcon img=new ImageIcon(url);
    setIconImage(img.getImage());
    
    
    
      try {
               DefaultTableModel dtm;
               Statement qry=cnx.conectar().createStatement();
               ResultSet rs,rs2;
               ResultSetMetaData ram;
               int y=0;
               rs2=qry.executeQuery("select concat(nombre,' ',apellidop) as nombre,c.examen as calificacion from alumnos join español as c on alumnos.idalumno=c.idalumno where idgrupo="+grupo+" group by alumnos.idalumno");
               
               
               
               while (rs2.next()) {
                   
              y++;
              
          }
               
           rs=qry.executeQuery("select alumnos.idalumno,concat(nombre,' ',apellidop) as nombre,c.examen as calificacion from alumnos join español as c on alumnos.idalumno=c.idalumno where idgrupo="+grupo+" group by alumnos.idalumno");
            
            String idd[]=new String[y];
            y=0;
               while (rs.next()) {
              idd[y]=rs.getString(1);
              y++;
          }
              
               String id2=idd[id];
              this.id=Integer.parseInt(id2);
               
               rs=qry.executeQuery("select * from alumnos  where idalumno="+id2);
                
               if (rs.next()) {
                   
              nombre.setText(rs.getString(2));
              apellido.setText(rs.getString(3));
              apellidom.setText(rs.getString(4));
              group.setText(rs.getString(5));  
          }
               
              
               dtm=(DefaultTableModel)this.jTable1.getModel();
              
            
               rs=qry.executeQuery("select c.nombreclub from alumnos as a join club_alumno using(idalumno) join clubes as c using(idclub) where idalumno="+id2);
               ram=rs.getMetaData();

               ArrayList<Object[]> data=new ArrayList<>();
               while(rs.next()){
               Object[] rows=new Object[ram.getColumnCount()];
               
               for(int i=0;i<rows.length;i++){
               rows[i]=rs.getObject(i+1);
               
               }
               data.add(rows);
               
               }
               
               for (int i = 0; i < data.size(); i++) {
                   dtm.addRow(data.get(i));
                   
               } 
               
               
               
               
               
               
               
               
           } catch (SQLException ex) {
               //JOptionPane.showMessageDialog(null, "error de sql");
           }
    
      
      
      
      
      
      
    
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
  


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        nombre = new javax.swing.JTextField();
        group = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        apellido = new javax.swing.JTextField();
        apellidom = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        ward = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());

        jLabel1.setText("Alumno");

        jButton1.setText("Cerrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        nombre.setText("jTextField1");
        nombre.setFocusable(false);

        group.setFocusable(false);

        jLabel2.setText("grupo");

        apellido.setText("jTextField2");
        apellido.setFocusable(false);

        apellidom.setText("jTextField3");
        apellidom.setFocusable(false);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "clubes"
            }
        ));
        jTable1.setEnabled(false);
        jTable1.setFocusable(false);
        jScrollPane1.setViewportView(jTable1);

        jLabel3.setText("Nombre(s)");

        jLabel4.setText("Apellido Paterno");

        jLabel5.setText("Apellido Materno");

        jButton2.setText("Editar informacion del alumno");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        ward.setText("ward cambios");
        ward.setEnabled(false);
        ward.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wardActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jButton2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 73, Short.MAX_VALUE)
                                .addComponent(ward)
                                .addGap(65, 65, 65)
                                .addComponent(jButton1))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(nombre, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
                                    .addComponent(jLabel2)
                                    .addComponent(group))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addGap(35, 35, 35)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(apellido, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel4))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel5)
                                            .addComponent(apellidom, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                        .addGap(42, 42, 42))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(apellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(apellidom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(group, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(ward))
                .addGap(33, 33, 33))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
dispose();        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        jTable1.setFocusable(true);
        jTable1.setEnabled(true);
        ward.setEnabled(true);
        group.setFocusable(true);
        nombre.setFocusable(true);
        apellido.setFocusable(true);
        apellidom.setFocusable(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void wardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wardActionPerformed
        try {
                JOptionPane.showMessageDialog(null,id);
               Statement qry=cnx.conectar().createStatement();
               ResultSet rs,rs2; 
               qry.executeUpdate("update alumnos set nombre='"+nombre.getText()+"', apellidop='"+apellido.getText()+"' "
                       + ", apellidom='"+apellidom.getText()+"'"
                       + " where idalumno="+id);
               JOptionPane.showMessageDialog(null,"hecho");
        }catch (SQLException ex) {
               JOptionPane.showMessageDialog(null, "error de sql "+ex);
           }
    }//GEN-LAST:event_wardActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField apellido;
    private javax.swing.JTextField apellidom;
    private javax.swing.JTextField group;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField nombre;
    private javax.swing.JButton ward;
    // End of variables declaration//GEN-END:variables

    

}
