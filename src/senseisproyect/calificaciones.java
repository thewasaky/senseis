/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package senseisproyect;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Pablo
 */
public class calificaciones extends javax.swing.JFrame {
private String grupo;
    /**
     * Creates new form calificaciones
     */
    public calificaciones(String grupo){
        initComponents();
    this.grupo=grupo;
    lblgrupo.setText(grupo);
     bd cnx=new bd();
      try {
               DefaultTableModel dtm;
               Statement qry=cnx.conectar().createStatement();
               ResultSet rs,rs2; 
               ResultSetMetaData ram;
               rs=qry.executeQuery("select materia from materias");
               materias.removeAllItems();
               while(rs.next()){
                 materias.addItem(rs.getString(1));
               }
               dtm=(DefaultTableModel)this.jTable1.getModel();
               int filas=jTable1.getRowCount();
            for (int i = 0;filas>i; i++) {
               dtm.removeRow(0);
               
            }
            String tabla="";
            
            if(materias.getSelectedItem().equals("español")){
                tabla="español";
      }
            if(materias.getSelectedItem().equals("matematicas")){
                tabla="pensamientomatematico";
      }
            if(materias.getSelectedItem().equals("conocimiento del medio")){
                tabla="conocimientodelmedio";
      }
            if(materias.getSelectedItem().equals("artes")){
                tabla="artes";
      }
            if(materias.getSelectedItem().equals("educacion socioemocional")){
                tabla="socioemocional";
      }
            if(materias.getSelectedItem().equals("educacion fisica")){
                tabla="edfisica";
      }         
            
            
            
            
            
            
            
            
            
            
     
            
            
            
            
            
            
               rs=qry.executeQuery("select concat(nombre,' ',apellidop) as nombre,examen as calificacion from alumnos join español as c on alumnos.idalumno=c.idalumno where idgrupo="+grupo+" group by alumnos.idalumno");
               
              // rs2=qry.executeQuery("select idalumnos from alumnos where idgrupo="+grupo);

               ram=rs.getMetaData();
               
               int numeroColumnas = ram.getColumnCount();
               Object[] etiquetas = new Object[numeroColumnas];

                // Se obtiene cada una de las etiquetas para cada columna
                for (int i = 0; i < numeroColumnas; i++)
                {
                  // Nuevamente, para ResultSetMetaData la primera columna es la 1. 
                  etiquetas[i] = ram.getColumnLabel(i + 1); 
                }

                dtm.setColumnIdentifiers(etiquetas);
                
               
               ArrayList<Object[]> data=new ArrayList<>();
               while(rs.next()){
               Object[] rows=new Object[ram.getColumnCount()];
               
               for(int i=0;i<rows.length;i++){
               rows[i]=rs.getObject(i+1);
               
               }
               data.add(rows);
               
               }
               
               for (int i = 0; i < data.size(); i++) {
                   dtm.addRow(data.get(i));
                   
               }
           } catch (SQLException ex) {
               //JOptionPane.showMessageDialog(null, "error de sql");
           }
    
    
    
    }
    public calificaciones() {
        initComponents();
        
        
         bd cnx=new bd();
      try {
               DefaultTableModel dtm;
               Statement qry=cnx.conectar().createStatement();
               ResultSet rs,rs2; 
               ResultSetMetaData ram;
               rs=qry.executeQuery("select materia from materias");
               materias.removeAllItems();
               while(rs.next()){
                 materias.addItem(rs.getString(1));
               }
               dtm=(DefaultTableModel)this.jTable1.getModel();
               int filas=jTable1.getRowCount();
            for (int i = 0;filas>i; i++) {
               dtm.removeRow(0);
               
            }
            String tabla="";
            
        if(materias.getSelectedItem().equals("español")){
                tabla="español";
      }
            if(materias.getSelectedItem().equals("matematicas")){
                tabla="pensamientomatematico";
      }
            if(materias.getSelectedItem().equals("conocimiento del medio")){
                tabla="conocimientodelmedio";
      }
            if(materias.getSelectedItem().equals("artes")){
                tabla="artes";
      }
            if(materias.getSelectedItem().equals("educacion socioemocional")){
                tabla="socioemocional";
      }
            if(materias.getSelectedItem().equals("educacion fisica")){
                tabla="edfisica";
      } 
            
            
            
            
            
            
            
            
            
            
     
            
            
            
            
            
            
               rs=qry.executeQuery("select concat(nombre,' ' ,apellidop) as nombre,examen as calificacion from alumnos join "+tabla+" on alumnos.idalumno="+tabla+".idalumno where idgrupo="+grupo+" group by alumnos.idalumno");
               
              // rs=qry.executeQuery("select nombre,apellidop,apellidom, c.nombreclub as club1, ca.nombreclub as club2,g.nomgrupo as grupo from alumnos join clubes as c on alumnos.idclub1=c.idclub join grupo as g using(idgrupo) join clubes as ca on alumnos.idclub2=ca.idclub where idgrupo='"+materias.getSelectedItem()+"'");

               ram=rs.getMetaData();
               
               int numeroColumnas = ram.getColumnCount();
               Object[] etiquetas = new Object[numeroColumnas];

                // Se obtiene cada una de las etiquetas para cada columna
                for (int i = 0; i < numeroColumnas; i++)
                {
                  // Nuevamente, para ResultSetMetaData la primera columna es la 1. 
                  etiquetas[i] = ram.getColumnLabel(i + 1); 
                }

                dtm.setColumnIdentifiers(etiquetas);
                
               
               ArrayList<Object[]> data=new ArrayList<>();
               while(rs.next()){
               Object[] rows=new Object[ram.getColumnCount()];
               
               for(int i=0;i<rows.length;i++){
               rows[i]=rs.getObject(i+1);
               
               }
               data.add(rows);
               
               }
               
               for (int i = 0; i < data.size(); i++) {
                   dtm.addRow(data.get(i));
                   
               }
           } catch (SQLException ex) {
               //JOptionPane.showMessageDialog(null, "error de sql");
           }
        
        
        
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        lblgrupo = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        materias = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ts"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel1.setText("grupo");

        jLabel3.setText("materia");

        materias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                materiasActionPerformed(evt);
            }
        });

        jButton1.setText("salir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("subir califas");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 675, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(85, 85, 85)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(lblgrupo)
                        .addGap(260, 260, 260)
                        .addComponent(jLabel3)
                        .addGap(49, 49, 49)
                        .addComponent(materias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton2)
                        .addGap(144, 144, 144)
                        .addComponent(jButton1)
                        .addGap(23, 23, 23)))
                .addContainerGap(35, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lblgrupo)
                    .addComponent(jLabel3)
                    .addComponent(materias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 70, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addGap(93, 93, 93))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        inicio ob1=new inicio();
        ob1.setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void materiasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_materiasActionPerformed
         bd cnx=new bd();
      try {
               DefaultTableModel dtm;
               Statement qry=cnx.conectar().createStatement();
               ResultSet rs,rs2; 
               ResultSetMetaData ram;
               
               dtm=(DefaultTableModel)this.jTable1.getModel();
               int filas=jTable1.getRowCount();
            for (int i = 0;filas>i; i++) {
               dtm.removeRow(0);
               
            }
            String tabla="";
            
           if(materias.getSelectedItem().equals("español")){
                tabla="español";
      }
            if(materias.getSelectedItem().equals("matematicas")){
                tabla="pensamientomatematico";
      }
            if(materias.getSelectedItem().equals("conocimiento del medio")){
                tabla="conocimientodelmedio";
      }
            if(materias.getSelectedItem().equals("artes")){
                tabla="artes";
      }
            if(materias.getSelectedItem().equals("educacion socioemocional")){
                tabla="socioemocional";
      }
            if(materias.getSelectedItem().equals("educacion fisica")){
                tabla="edfisica";
      } 
            
            
            
            
            
            
            
            
            
            
     
            
            
            
            
            
            
              
               rs=qry.executeQuery("select concat(nombre,' ',apellidop) as nombre,c.examen as calificacion from alumnos join "+tabla+" as c on alumnos.idalumno=c.idalumno where idgrupo="+grupo+" group by alumnos.idalumno");
               
              // rs=qry.executeQuery("select nombre,apellidop,apellidom, c.nombreclub as club1, ca.nombreclub as club2,g.nomgrupo as grupo from alumnos join clubes as c on alumnos.idclub1=c.idclub join grupo as g using(idgrupo) join clubes as ca on alumnos.idclub2=ca.idclub where idgrupo='"+materias.getSelectedItem()+"'");

               ram=rs.getMetaData();
               
               int numeroColumnas = ram.getColumnCount();
               Object[] etiquetas = new Object[numeroColumnas];

                // Se obtiene cada una de las etiquetas para cada columna
                for (int i = 0; i < numeroColumnas; i++)
                {
                  // Nuevamente, para ResultSetMetaData la primera columna es la 1. 
                  etiquetas[i] = ram.getColumnLabel(i + 1); 
                }

                dtm.setColumnIdentifiers(etiquetas);
                
               
               ArrayList<Object[]> data=new ArrayList<>();
               while(rs.next()){
               Object[] rows=new Object[ram.getColumnCount()];
               
               for(int i=0;i<rows.length;i++){
               rows[i]=rs.getObject(i+1);
               
               }
               data.add(rows);
               
               }
               
               for (int i = 0; i < data.size(); i++) {
                   dtm.addRow(data.get(i));
                   
               }
           } catch (SQLException ex) {
               //JOptionPane.showMessageDialog(null, "error de sql");
           }
        
        
        
    }//GEN-LAST:event_materiasActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        String tabla="";
            
            if(materias.getSelectedItem().equals("español")){
                tabla="español";
      }
            if(materias.getSelectedItem().equals("matematicas")){
                tabla="pensamientomatematico";
      }
            if(materias.getSelectedItem().equals("conocimiento del medio")){
                tabla="conocimientodelmedio";
      }
            if(materias.getSelectedItem().equals("artes")){
                tabla="artes";
      }
            if(materias.getSelectedItem().equals("educacion socioemocional")){
                tabla="socioemocional";
      }
            if(materias.getSelectedItem().equals("educacion fisica")){
                tabla="edfisica";
      } 
        
        
        bd cnx=new bd();
      try {
               DefaultTableModel dtm;
               Statement qry=cnx.conectar().createStatement();
               ResultSet rs,rs2; 
               ResultSetMetaData ram;
               rs=qry.executeQuery("select idalumno from alumnos where idgrupo="+grupo+" order by idalumno ;");
             int t=1;
             while(rs.next()){
                 t++;
                 
             }
             String[] d=new String[t];
             int y=0;
         rs2=qry.executeQuery("select idalumno from alumnos where idgrupo="+grupo+" order by idalumno ;");
        while(rs2.next()){
        d[y]=rs2.getString(1);
        
        y++;
        }
        for(int i=0;i<jTable1.getRowCount();i++){
            String re;
           re= jTable1.getValueAt(i,1).toString();
                
        qry.executeUpdate("update "+tabla+" set examen="+re+" where idalumno='"+d[i]+"'");
            
          
        
       
        
       }
        JOptionPane.showMessageDialog(null, "OKAS");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        if(evt.getClickCount()==2){
         int x=jTable1.getSelectedRow();                                           
         infoAlumno ob=new infoAlumno(x, Integer.parseInt(grupo));
         ob.setVisible(true);
         
       }
    }//GEN-LAST:event_jTable1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(calificaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(calificaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(calificaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(calificaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new calificaciones().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel lblgrupo;
    private javax.swing.JComboBox<String> materias;
    // End of variables declaration//GEN-END:variables
}
